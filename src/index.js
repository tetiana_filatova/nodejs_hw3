const dotenv = require('dotenv');
dotenv.config();
const express = require('express');
const path = require('path');
const morgan = require('morgan');
const mongoose = require('mongoose');

const app = express();

const port = process.env.PORT || 8080;
const {authRouter} = require('./controllers/authController');
const {trucksRouter} = require('./controllers/trucksController');
const {loadsRouter} = require('./controllers/loadsController');
const {profileRouter} = require('./controllers/profileController');
const {authMiddleware} = require('./middlewares/authMiddleware');
const {permit} = require('./middlewares/roleMiddleware');
const {UberTruckApiError, InvalidPathError} = require('./utils/errors');

app.use(express.json());
app.use(morgan('tiny'));

app.use(express.static(path.join(__dirname, 'public')));

app.use('/api/auth', authRouter);
app.use('/api/trucks', [authMiddleware, permit('DRIVER')], trucksRouter);
app.use('/api/loads', [authMiddleware], loadsRouter);
app.use('/api/users/me', [authMiddleware], profileRouter);

app.get('/api/documentation', (req, res) => {
  res.sendFile(path.join(__dirname, './public', 'userApi.html'));
});

app.use((req, res, next) => {
  throw new InvalidPathError('Not found');
});

app.use((err, req, res, next) => {
  if (err instanceof UberTruckApiError) {
    return res.status(err.status).json({message: err.message});
  }
  res.status(500).json({message: err.message});
});

const start = async () => {
  try {
    await mongoose.connect('mongodb+srv://user1:useruser@cluster0.7t0pq.mongodb.net/ubertruck?retryWrites=true&w=majority', {
      useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true,
      useFindAndModify: false,
    });

    app.listen(port);
  } catch (err) {
    console.error(`Error on server startup: ${err.message}`);
  }
};

start();
