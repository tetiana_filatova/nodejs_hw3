const mongoose = require('mongoose');

const Truck = mongoose.model('Truck', {
  created_by: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
  },
  assigned_to: {
    type: String,
    default: null,
  },
  type: {
    type: String,
    enum: ['SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT'],
    required: true,
  },
  status: {
    type: String,
    enum: ['IS', 'OL'],
    default: 'IS',
  },
  deleted: {
    type: Boolean,
    default: false,
  },

  createdDate: {
    type: Date,
    default: Date.now(),
  },
});

module.exports = {Truck};
