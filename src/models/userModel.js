const mongoose = require('mongoose');

const User = mongoose.model('User', {
  role: {
    type: String,
    enum: ['DRIVER', 'SHIPPER'],
    required: true,
  },
  email: {
    type: String,
    required: true,
    unique: true,
  },
  password: {
    type: String,
    required: true,
  },
  profileImage: String,
  deleted: {
    type: Boolean,
    default: false,
  },

  created_date: {
    type: Date,
    default: Date.now(),
  },
});

module.exports = {User};
