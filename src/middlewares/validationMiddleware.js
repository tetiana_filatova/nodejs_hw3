const Joi = require('joi');

const registrationValidator = async (req, res, next) => {
  const schema = Joi.object({
    email: Joi.string()
        .email()
        .required(),
    password: Joi.string()
        .min(6)
        .max(20)
        .required(),
    role: Joi.string()
        .valid('DRIVER', 'SHIPPER')
        .required(),
  });

  try {
    await schema.validateAsync(req.body);
    next();
  } catch (err) {
    next(err);
  }
};

const loginValidator = async (req, res, next) => {
  const schema = Joi.object({
    email: Joi.string()
        .email()
        .required(),
    password: Joi.string()
        .required(),
  });

  try {
    await schema.validateAsync(req.body);
    next();
  } catch (err) {
    next(err);
  }
};

const truckValidator = async (req, res, next) => {
  const schema = Joi.object({
    type: Joi.string()
        .valid('SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT')
        .required(),
  });

  try {
    await schema.validateAsync(req.body);
    next();
  } catch (err) {
    next(err);
  }
};

const loadValidator = async (req, res, next) => {
  const schema = Joi.object({
    name: Joi.string()
        .required(),
    payload: Joi.number()
        .required(),
    pickup_address: Joi.string()
        .required(),
    delivery_address: Joi.string()
        .required(),
    dimensions: Joi.object({
      width: Joi.number()
          .required(),
      length: Joi.number()
          .required(),
      height: Joi.number()
          .required(),
    }),
  });

  try {
    await schema.validateAsync(req.body);
    next();
  } catch (err) {
    next(err);
  }
};

module.exports = {
  registrationValidator,
  loginValidator,
  truckValidator,
  loadValidator,
};
