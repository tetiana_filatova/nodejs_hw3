const {getRole} = require('../services/roleService');
// const {InvalidRoleError} = require('../utils/errors');

const permit = (...permittedRoles) => {
  // return a middleware
  return async (request, response, next) => {
    const {user} = request;
    const role = await getRole(user.email);

    if (user && permittedRoles.includes(role)) {
      next(); // role is allowed, so continue on the next middleware
    } else {
      response.status(403).json({message: 'Forbidden'}); // user is forbidden
      // throw new InvalidRoleError();
    }
  };
};

module.exports = {
  permit,
};
