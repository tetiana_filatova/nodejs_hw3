const loadLogger = (message) => {
  return {
    message,
    time: new Date(Date.now()),
  };
};

module.exports = {
  loadLogger,
};
