class UberTruckApiError extends Error {
  constructor(message) {
    super(message);
    this.status = 500;
  }
}

class InvalidRequestError extends UberTruckApiError {
  constructor(message = 'Invalid request') {
    super(message);
    this.status = 400;
  }
}

class InvalidCredentialsError extends UberTruckApiError {
  constructor(message = 'Not authorized') {
    super(message);
    this.status = 401;
  }
}

class InvalidPathError extends UberTruckApiError {
  constructor(message = 'Page not found') {
    super(message);
    this.status = 404;
  }
}

class InvalidRoleError extends UberTruckApiError {
  constructor(message = 'Forbidden') {
    super(message);
    this.status = 403;
  }
}

module.exports = {
  UberTruckApiError,
  InvalidRequestError,
  InvalidCredentialsError,
  InvalidPathError,
  InvalidRoleError,
};
