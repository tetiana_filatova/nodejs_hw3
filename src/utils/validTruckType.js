const truckTypes = [{
  type: 'SPRINTER',
  dimensions: {
    width: 300,
    length: 250,
    height: 170,
  },
  payload: 1700,
},
{
  type: 'SMALL STRAIGHT',
  dimensions: {
    width: 500,
    length: 250,
    height: 170,
  },
  payload: 2500,
},
{
  type: 'LARGE STRAIGHT',
  dimensions: {
    width: 700,
    length: 350,
    height: 200,
  },
  payload: 4000,
}];

const validTruckType = (dims, payload) => {
  const filtration = (item) => item.dimensions.width > dims.width &&
    item.dimensions.length > dims.length && item.dimensions.height >
    dims.height && item.payload > payload;
  return truckTypes.filter(filtration).length > 1 ?
    truckTypes.filter(filtration).map((elem) => elem.type) : [];
};

module.exports = {
  validTruckType,
};
