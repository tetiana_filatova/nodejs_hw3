const express = require('express');
const router = new express.Router();

const {
  getTrucksByUserId,
  getTruckByIdForUser,
  addTruckToUser,
  updateTruckByIdForUser,
  deleteTruckByIdForUser,
  assignTruckByIdForUser,
} = require('../services/trucksService');

const {
  asyncWrapper,
} = require('../utils/apiUtils');

const {
  truckValidator,
} = require('../middlewares/validationMiddleware');

const {
  InvalidRequestError,
} = require('../utils/errors');

router.get('/', asyncWrapper(async (req, res) => {
  const {userId} = req.user;

  const trucks = await getTrucksByUserId(userId);
  res.json({trucks});
}));

router.get('/:id', asyncWrapper(async (req, res) => {
  const {userId} = req.user;
  const {id} = req.params;

  const truck = await getTruckByIdForUser(id, userId);

  if (!truck) {
    throw new InvalidRequestError('No truck with such id found!');
  }

  res.json({truck});
}));

router.post('/', truckValidator, asyncWrapper(async (req, res) => {
  const {userId} = req.user;

  await addTruckToUser(userId, req.body);

  res.json({message: 'Truck created successfully'});
}));

router.put('/:id', truckValidator, asyncWrapper(async (req, res) => {
  const {userId} = req.user;
  const {id} = req.params;
  const data = req.body;

  const updatedTruck = await updateTruckByIdForUser(id, userId, data);

  if (!updatedTruck) {
    throw new InvalidRequestError('You can not update truck assigned to you');
  }

  res.json({message: 'Truck details changed successfully'});
}));

router.delete('/:id', asyncWrapper(async (req, res) => {
  const {userId} = req.user;
  const {id} = req.params;
  const deletedTruck = await deleteTruckByIdForUser(id, userId);

  if (!deletedTruck) {
    throw new InvalidRequestError('You can not delete truck assigned to you');
  }

  res.json({message: 'Truck deleted successfully'});
}));

router.post('/:id/assign', asyncWrapper(async (req, res) => {
  const {userId} = req.user;
  const {id} = req.params;

  await assignTruckByIdForUser(id, userId);

  res.json({message: 'Truck assigned successfully'});
}));

module.exports = {
  trucksRouter: router,
};
