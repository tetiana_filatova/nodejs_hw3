const express = require('express');
const router = new express.Router();
const multer = require('multer');
const path = require('path');
const upload = multer({dest: path.join(__dirname, '../public/uploads/images')});

const {
  viewProfile,
  deleteProfile,
  changePassword,
  addPhoto,
} = require('../services/profileService');

const {
  InvalidRequestError,
} = require('../utils/errors');

const {
  asyncWrapper,
} = require('../utils/apiUtils');

router.get('/', asyncWrapper(async (req, res) => {
  const {userId} = req.user;

  const user = await viewProfile(userId);
  if (!user) {
    throw new InvalidRequestError();
  }

  res.json({user});
}));

router.patch('/', asyncWrapper(async (req, res) => {
  const {userId} = req.user;
  const oldPassword = req.body.oldPassword;
  const newPassword = req.body.newPassword;

  await changePassword(userId, oldPassword, newPassword);

  res.json({message: 'Password changed successfully'});
}));

router.delete('/', asyncWrapper(async (req, res) => {
  const {userId} = req.user;

  await deleteProfile(userId);

  res.json({message: 'Profile deleted successfully'});
}));

router.get('/upload', (req, res) => {
  res.sendFile(path.join(__dirname, '../public', 'index.html'));
});


router.post('/upload', upload.single('photo'),
    asyncWrapper(async (req, res) => {
      const {userId} = req.user;
      if (req.file) {
        await addPhoto(userId, req.file.filename);
        res.json({message: 'Avatar was successfully uploaded'});
      } else {
        throw new InvalidRequestError();
      }
    }));

router.get('/image/:filename', (req, res) => {
  const {filename} = req.params;
  const fullfilepath = path.join(__dirname, '../public/uploads/images/',
      filename);
  return res.sendFile(fullfilepath);
});

module.exports = {
  profileRouter: router,
};
