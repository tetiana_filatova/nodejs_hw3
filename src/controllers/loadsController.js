const express = require('express');
const router = new express.Router();

const {
  getLoadsByShipperId,
  getLoadsByDriverId,
  getLoadByIdForUser,
  addLoadToUser,
  updateLoadByIdForUser,
  deleteLoadByIdForUser,
  findTruckForLoadByLoadId,
  getActiveLoadForDriverByDriverId,
  updateLoadStateForDriverByDriverId,
  getShippingInfoForShipperByActiveLoadId,
} = require('../services/loadsService');

const {
  asyncWrapper,
} = require('../utils/apiUtils');
const {permit} = require('../middlewares/roleMiddleware');
const {getRole} = require('../services/roleService');

const {
  loadValidator,
} = require('../middlewares/validationMiddleware');

const {
  InvalidRequestError,
  InvalidRoleError,
} = require('../utils/errors');

router.get('/', asyncWrapper(async (req, res) => {
  const {userId, email} = req.user;

  const role = await getRole(email);
  const status = req.query.status;
  const limit = req.query.limit && parseInt(req.query.limit) <= 50 ?
        parseInt(req.query.limit) : 10;
  const offset = req.query.offset ? parseInt(req.query.offset) : 0;
  if (status === 'NEW' || status === 'POSTED' && role === 'DRIVER') {
    throw new InvalidRoleError();
  }
  let loads;
  if (role === 'SHIPPER') {
    loads = await getLoadsByShipperId(userId, status, offset, limit);
  }
  if (role === 'DRIVER') {
    loads = await getLoadsByDriverId(userId, status, offset, limit);
  }
  res.json({loads});
}));

router.get('/active', [permit('DRIVER')], asyncWrapper(async (req, res) => {
  const {userId} = req.user;

  const load = await getActiveLoadForDriverByDriverId(userId);

  res.json({load});
}));

router.patch('/active/state', [permit('DRIVER')],
    asyncWrapper(async (req, res) => {
      const {userId} = req.user;

      const newState = await updateLoadStateForDriverByDriverId(userId);

      res.json({message: `Load state changed to '${newState}'`});
    }));

router.get('/:id', asyncWrapper(async (req, res) => {
  const {userId} = req.user;
  const {id} = req.params;

  const load = await getLoadByIdForUser(id, userId);

  if (!load) {
    throw new InvalidRequestError('No load with such id found!');
  }

  res.json({load});
}));

router.post('/', [permit('SHIPPER')], loadValidator,
    asyncWrapper(async (req, res) => {
      const {userId} = req.user;

      await addLoadToUser(userId, req.body);

      res.json({message: 'Load created successfully'});
    }));

router.put('/:id', [permit('SHIPPER')], loadValidator,
    asyncWrapper(async (req, res) => {
      const {userId} = req.user;
      const {id} = req.params;
      const data = req.body;

      const updatedLoad = await updateLoadByIdForUser(id, userId, data);

      if (!updatedLoad) {
        throw new InvalidRequestError('Only "NEW" load can be updated');
      }

      res.json({message: 'Load details changed successfully'});
    }));

router.delete('/:id', [permit('SHIPPER')], asyncWrapper(async (req, res) => {
  const {userId} = req.user;
  const {id} = req.params;
  const deletedLoad = await deleteLoadByIdForUser(id, userId);

  if (!deletedLoad) {
    throw new InvalidRequestError('Only "NEW" load can be deleted');
  }

  res.json({message: 'Load deleted successfully'});
}));

router.post('/:id/post', [permit('SHIPPER')], asyncWrapper(async (req, res) => {
  const {userId} = req.user;
  const {id} = req.params;
  const isFound = await findTruckForLoadByLoadId(id, userId);
  const message = isFound ? 'Load posted successfully' : 'Please try again';

  res.json({
    message: message,
    driver_found: isFound,
  });
}));

router.get('/:id/shipping_info', [permit('SHIPPER')],
    asyncWrapper(async (req, res) => {
      const {userId} = req.user;
      const {id} = req.params;
      const info = await getShippingInfoForShipperByActiveLoadId(id, userId);
      res.json({
        load: info.load,
        driver: info.truck,
      });
    }));

module.exports = {
  loadsRouter: router,
};
