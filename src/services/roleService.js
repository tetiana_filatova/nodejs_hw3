const {User} = require('../models/userModel');

const {
  InvalidRequestError,
} = require('../utils/errors');

const getRole = async (email) => {
  const user = await User.findOne({email});

  if (!user) {
    throw new InvalidRequestError('Invalid username or password');
  }
  return user.role;
};

module.exports = {
  getRole,
};
