const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const {User} = require('../models/userModel');

const {
  InvalidRequestError,
} = require('../utils/errors');

const registration = async ({email, password, role}) => {
  const roles = ['DRIVER', 'SHIPPER'];
  if (roles.includes(role)) {
    const user = new User({
      email,
      password: await bcrypt.hash(password, 10),
      role,
    });
    await user.save();
  } else {
    throw new InvalidRequestError('Invalid role');
  }
};

const logIn = async ({email, password}) => {
  const user = await User.findOne({email});

  if (!user) {
    throw new InvalidRequestError('Invalid username or password');
  }

  if (!(await bcrypt.compare(password, user.password))) {
    throw new InvalidRequestError('Invalid username or password');
  }

  // should be changed for the deployment
  const devSecret = 'Ubertrucks';

  const token = jwt.sign({
    _id: user._id,
    email: user.email,
  }, devSecret);
  return token;
};

module.exports = {
  registration,
  logIn,
};
