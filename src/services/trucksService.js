const {Truck} = require('../models/truckModel');

const {
  InvalidRequestError,
} = require('../utils/errors');

const getTrucksByUserId = async (created_by) => {
  const trucks = await Truck.find({created_by, deleted: false},
      '-__v -deleted');
  return trucks;
};

const getTruckByIdForUser = async (truckId, created_by) => {
  const truck = await Truck.findOne({_id: truckId, created_by, deleted: false},
      '-__v -deleted');
  return truck;
};

const addTruckToUser = async (created_by, truckPayload) => {
  const truck = new Truck({...truckPayload, created_by});
  await truck.save();
};

const updateTruckByIdForUser = async (truckId, created_by, data) => {
  const updatedTruck = await Truck.findOneAndUpdate({_id: truckId, created_by,
    assigned_to: {$ne: created_by}}, {$set: data});
  return updatedTruck;
};

const deleteTruckByIdForUser = async (truckId, created_by) => {
  try {
    const deletedTruck = await Truck.findOneAndRemove({_id: truckId,
      created_by, assigned_to: {$ne: created_by}});
    return deletedTruck;
  } catch (err) {
    throw new InvalidRequestError(err.message);
  }
};

const assignTruckByIdForUser = async (truckId, created_by) => {
  try {
    const prevAssignedTruck = await Truck.findOne({created_by,
      assigned_to: created_by});
    const currAssignedTruck = await Truck.findOne({_id: truckId, created_by});
    if (prevAssignedTruck?._id !== truckId && currAssignedTruck) {
      await Truck.updateOne({created_by, assigned_to: created_by},
          {$set: {assigned_to: null}});
      await Truck.updateOne({_id: truckId, created_by},
          {$set: {assigned_to: created_by}});
    }
  } catch (err) {
    throw new InvalidRequestError(err.message);
  }
};

module.exports = {
  getTrucksByUserId,
  getTruckByIdForUser,
  addTruckToUser,
  updateTruckByIdForUser,
  deleteTruckByIdForUser,
  assignTruckByIdForUser,
};
