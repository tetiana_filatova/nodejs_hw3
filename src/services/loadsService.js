const {Load} = require('../models/loadModel');
const {Truck} = require('../models/truckModel');


const {validTruckType} = require('../utils/validTruckType');
const {loadLogger} = require('../utils/loadLogger');

const {
  InvalidRequestError,
} = require('../utils/errors');

const getLoadsByShipperId = async (shipperId, status={$in:
  ['NEW', 'POSTED', 'ASSIGNED', 'SHIPPED']}, offset=0, limit=10) => {
  // const count = await Load.find({created_by, status}).count();
  const loads = await Load.find({created_by: shipperId, status, deleted: false},
      '-__v -deleted')
      .skip(offset)
      .limit(limit);
  return loads;
};

const getLoadsByDriverId = async (driverId, status={$in:
  ['ASSIGNED', 'SHIPPED']}, offset=0, limit=10) => {
  // const count = await Load.find({created_by, status}).count();
  // console.log(driverId, status, offset, limit)
  const loads = await Load.find({assigned_to: driverId, status, deleted: false},
      '-__v -deleted')
      .skip(offset)
      .limit(limit);
  return loads;
};

const getLoadByIdForUser = async (loadId, created_by) => {
  const load = await Load.findOne({_id: loadId, created_by, deleted: false},
      '-__v -deleted');
  return load;
};

const addLoadToUser = async (created_by, loadPayload) => {
  const load = new Load({...loadPayload, created_by});
  await load.save();
};

const updateLoadByIdForUser = async (loadId, created_by, data) => {
  const updatedLoad = await Load.findOneAndUpdate({_id: loadId, created_by,
    status: 'NEW'}, {$set: data});
  return updatedLoad;
};

const deleteLoadByIdForUser = async (loadId, created_by) => {
  try {
    const deletedLoad = await Load.findOneAndRemove({_id: loadId,
      created_by, status: 'NEW'});
    return deletedLoad;
  } catch (err) {
    throw new InvalidRequestError(err.message);
  }
};

const getLogArrayByLoadId = async (loadId, created_by) => {
  const load = await Load.findOne({_id: loadId, created_by, deleted: false});
  return load.logs;
};

const findTruckForLoadByLoadId = async (loadId, created_by) => {
  const postedStatus = loadLogger('Load status was changed to POSTED');
  const newLogs = await getLogArrayByLoadId(loadId, created_by);
  newLogs.push(postedStatus);
  const load = await Load.findOneAndUpdate({_id: loadId, created_by,
    status: 'NEW', deleted: false}, {$set: {status: 'POSTED', logs: newLogs}});
  if (load) {
    const truckTypeArr = validTruckType(load.dimensions, load.payload);
    // console.log(truckTypeArr);
    if (truckTypeArr.length === 0) {
      const newStatus = loadLogger('Load status was changed to NEW');
      const newLogs = await getLogArrayByLoadId(loadId, created_by);
      newLogs.push(newStatus);
      await Load.updateOne({_id: loadId, created_by},
          {$set: {status: 'NEW', logs: newLogs}});
      throw new InvalidRequestError('Sorry, your load does not fit our Trucks');
    }
    const truck = await Truck.findOne({status: 'IS', assigned_to: {$ne: null},
      type: {$in: truckTypeArr}, deleted: false});
    // console.log('We found truck:', truck);
    // console.log('Found truck id:', truck._id);
    if (truck) {
      const activeTruck = await Truck.findOneAndUpdate({_id: truck._id},
          {$set: {status: 'OL'}});
      // console.log('Active truck:', activeTruck);
      const assignedStatus = loadLogger(`Load assigned to driver with id
        ${activeTruck.assigned_to}`);
      const deliveryState =
        loadLogger(`Load state changed to 'En route to Pick Up'`);
      const newLogs = await getLogArrayByLoadId(loadId, created_by);
      newLogs.push(assignedStatus, deliveryState);
      await Load.updateOne({_id: loadId, created_by}, {$set:
        {status: 'ASSIGNED', state: 'En route to Pick Up',
          assigned_to: activeTruck?.assigned_to, logs: newLogs}});
      return true;
    } else {
      const newStatus = loadLogger('Load status was changed to NEW');
      const newLogs = await getLogArrayByLoadId(loadId, created_by);
      newLogs.push(newStatus);
      await Load.updateOne({_id: loadId, created_by},
          {$set: {status: 'NEW', logs: newLogs}});
      return false;
    }
  } else {
    throw new InvalidRequestError('Sorry, no load found to post');
  }
};

const getActiveLoadForDriverByDriverId = async (driverId) => {
  const activeLoad = await Load.findOne({status: 'ASSIGNED',
    assigned_to: driverId}, '-__v -deleted');
  return activeLoad || [];
};

const updateLoadStateForDriverByDriverId = async (driverId) => {
  const state = ['En route to Pick Up', 'Arrived to Pick Up',
    'En route to delivery', 'Arrived to delivery'];
  try {
    const activeLoad = await Load.findOne({status: 'ASSIGNED',
      assigned_to: driverId, deleted: false});
    let newState;
    if (activeLoad) {
      const currState = activeLoad.state;
      const newStateIndex = state.indexOf(currState) + 1;
      if (newStateIndex <= 3 && newStateIndex > 0) {
        newState = state[newStateIndex];
        const deliveryState = loadLogger(`Load state changed to ${newState}`);
        const newLogs = await getLogArrayByLoadId(activeLoad._id,
            activeLoad.created_by);
        newLogs.push(deliveryState);
        await Load.updateOne({assigned_to: driverId}, {$set: {state: newState,
          logs: newLogs}});
      }
      if (newState === 'Arrived to delivery') {
        const shippedStatus = loadLogger('Load status was changed to SHIPPED');
        const newLogs = await getLogArrayByLoadId(activeLoad._id,
            activeLoad.created_by);
        newLogs.push(shippedStatus);
        await Load.updateOne({assigned_to: driverId}, {$set: {status: 'SHIPPED',
          logs: newLogs}});
        await Truck.updateOne({assigned_to: driverId}, {$set: {status: 'IS'}});
      }
      return newState;
    } else {
      throw new InvalidRequestError('No active loads found');
    }
  } catch (err) {
    throw new InvalidRequestError(err.message);
  }
};

const getShippingInfoForShipperByActiveLoadId = async (loadId, created_by) => {
  const load = await Load.findOne({_id: loadId, created_by, status: 'ASSIGNED'},
      '-__v -deleted');
  if (!load) {
    throw new InvalidRequestError('Sorry, this load is not active');
  }
  const truck = await Truck.findOne({assigned_to: load.assigned_to},
      '-__v -deleted');
  return {load, truck};
};

module.exports = {
  getLoadsByShipperId,
  getLoadsByDriverId,
  getLoadByIdForUser,
  addLoadToUser,
  updateLoadByIdForUser,
  updateLoadStateForDriverByDriverId,
  deleteLoadByIdForUser,
  findTruckForLoadByLoadId,
  getActiveLoadForDriverByDriverId,
  getShippingInfoForShipperByActiveLoadId,
};
