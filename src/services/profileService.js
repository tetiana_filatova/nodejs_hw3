const {User} = require('../models/userModel');
const {Truck} = require('../models/truckModel');
const {Load} = require('../models/loadModel');
const bcrypt = require('bcrypt');

const {
  InvalidRequestError,
} = require('../utils/errors');

const viewProfile = async (userId) => {
  const user = await User.findById({_id: userId}, '-password -__v -deleted');
  return user;
};

const deleteProfile = async (userId) => {
  const user = await User.findOne({_id: userId});
  if (user.role === 'DRIVER') {
    const olTruck = await Truck.findOne({assigned_to: userId, status: 'OL'});
    if (olTruck) {
      throw new InvalidRequestError('Deliver load before deleting profile');
    }
  }
  const targetModel = user.role === 'DRIVER' ? Truck : Load;
  await targetModel.updateMany({created_by: userId}, {$set: {deleted: true}});
  await User.updateOne({_id: userId}, {$set: {deleted: true}});
};

const changePassword = async (userId, oldPassword, newPassword) => {
  const user = await User.findOne({_id: userId});

  if (!(await bcrypt.compare(oldPassword, user.password))) {
    throw new Error('Invalid current password');
  }

  await User.updateOne({_id: userId}, {$set: {password:
    await bcrypt.hash(newPassword, 10)}});
};

const addPhoto = async (userId, imgName) => {
  await User.updateOne({_id: userId}, {$set: {profileImage: imgName}});
};

module.exports = {
  viewProfile,
  deleteProfile,
  changePassword,
  addPhoto,
};
